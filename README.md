# fullstaskell1

**Fullstack1 is WIP, see** [fullstaskell2](https://gitlab.com/juliendehos/fullstaskell2)

FULLSTack hASKELL (with reflex-platform).

[Project development with reflex-platform.](https://github.com/reflex-frp/reflex-platform/blob/develop/docs/project-development.md)

## build

- build all :

```
$ nix-build
```

- individual builds :

```
$ nix-build -o result-backend -A ghc.backend
$ nix-build -o result-frontend-reflex -A ghcjs.frontend_reflex

$ nix-build -o result-common -A ghc.common
$ nix-build -o result-common-ghcjs -A ghcjs.common
$ nix-build -o result-frontend-reflex-ghc -A ghc.frontend_reflex
```

## shell

- build with cabal/ghc :

```
$ nix-shell -A shells.ghc
[nix-shell]$ cabal new-build all
```

- build with cabal/ghcjs :

```
$ nix-shell -A shells.ghcjs
[nix-shell]$ cabal --project-file=cabal-ghcjs.project --builddir=dist-ghcjs new-build all
```

## TODO

- frontend_miso

- mobile app

- deployment, static files...

