{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson (eitherDecodeStrict)
import JavaScript.Web.XMLHttpRequest 
import Miso
import Miso.String (pack, toMisoString)

import Common (Square (..))

type EitherSquare = Either String Square 

data Model = Model { modelSquare :: EitherSquare } deriving (Eq, Show)

data Action = FetchSquare | SetSquare EitherSquare | NoOp deriving (Show, Eq)

main :: IO ()
main = startApp App 
    { model = Model (Left "")
    , initialAction = NoOp
    , mountPoint = Nothing
    , update = updateModel
    , events = defaultEvents
    , subs = []
    , view = viewModel
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel FetchSquare m = m <# (SetSquare <$> queryApi)
updateModel (SetSquare eitherSquare) m = noEff m { modelSquare = eitherSquare }
updateModel NoOp m = noEff m

viewModel :: Model -> View Action
viewModel (Model info) = div_ []
    [ h1_ [] [ text "Compute Square" ]
    , button_ 
        [ onClick FetchSquare ]
        [ text "Fetch data" ]
    , p_ [] $ case info of 
        Left msg -> [ text $ pack msg ]
        Right (Square x x2 _) -> [ text (toMisoString x), text "^2 = ", text (toMisoString x2) ]
    ]

queryApi :: IO EitherSquare
queryApi = do
    Just resp <- contents <$> xhrByteString req
    return $ eitherDecodeStrict resp
    where
        req = Request { reqMethod = GET
                      , reqURI = "http://localhost:3000/42"
                      , reqLogin = Nothing
                      , reqHeaders = []
                      , reqWithCredentials = False
                      , reqData = NoData
                      }

