{-# LANGUAGE OverloadedStrings #-}

import Common
import Data.Aeson (decode, encode)

main :: IO ()
main = do
    let cs42 = computeSquare 42
        str42 = "{ \"squareX\": 42, \"squareX2\": 1764, \"squareInfo\": \"from main\" }"
        dec42 = decode str42 :: Maybe Square
        enc42 = encode cs42
    print dec42
    print enc42

