
{-# LANGUAGE OverloadedStrings #-}
import Reflex
import Reflex.Dom
import Data.Default
import Control.Lens
import Data.Maybe

import qualified Data.Text as T

main :: IO ()
main = mainWidget $ el "div" $ do
    let req = xhrRequest "GET" "http://localhost:3000/42" def
    pb <- getPostBuild
    asyncReq <- performRequestAsync (tag (constant req) pb)
    resp <- holdDyn (Nothing :: Maybe T.Text ) $ fmap _xhrResponse_responseText asyncReq
    text "Response: "
    display resp
    -- dynText =<< mapDyn (fromMaybe "nope" . fmap fromJSString) resp




{-

-- https://stackoverflow.com/questions/30264504/xhrrequest-with-reflex-reflex-dom
{-# LANGUAGE OverloadedStrings #-}
import Reflex (holdDyn)
import Reflex.Dom (button, el, mainWidget, display)
import Reflex.Dom.Xhr (performRequestAsync, xhrRequest, decodeXhrResponse, _xhrResponse_responseText)
import Reflex.Class (tag, constant)

import Data.Default (def)
import qualified Data.Text as T

main :: IO ()
main = do
  mainWidget $ el "div" $ do
    buttonEvent <- button "click me"
    let defaultReq = xhrRequest "GET" "http://localhost:3000/42" def
    asyncEvent <- performRequestAsync (tag (constant defaultReq) buttonEvent)
    buttonDyn <- holdDyn (Just "default" :: Maybe String) $ fmap decodeXhrResponse asyncEvent
    display buttonDyn

-}


{-

-- https://www.reddit.com/r/reflexfrp/comments/3b410y/help_making_xhr_requests/
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import Data.ByteString.Lazy
import Data.Text
import Data.Default (def)

import GHC.Generics

import Reflex (holdDyn)
import Reflex.Class (tag, constant)
import Reflex.Dom (button, el, mainWidget, display)
import Reflex.Dom.Xhr (performRequestAsync, xhrRequest, decodeXhrResponse)

data Response = Response { answer :: Text} deriving (Show, Eq, Read, Generic)
instance ToJSON Response
instance FromJSON Response

main :: IO ()
main = do
  mainWidget $ el "div" $ do
    buttonEvent <- button "click me"
    let defaultReq = xhrRequest "GET" "http://localhost:3000/42" def
    asyncEvent <- performRequestAsync (tag (constant defaultReq) buttonEvent)
    buttonDyn <- holdDyn (Just (Response "Nothing yet")) $ fmap decodeXhrResponse asyncEvent
    display buttonDyn

-}


{-

import Control.Applicative ((<*>), (<$>))
import Control.Monad (liftM)
import Data.Aeson (decode)
import Data.Map (Map)
import Data.Text (pack, unpack)
import Network.HTTP.Conduit (simpleHttp)
import Reflex
import Reflex.Dom
import Text.Read (readMaybe)

import Common (mysquare)

main = mainWidget $ el "div" $ do
    nx <- numberInput
    el "p" $ do
        let result = (liftM mysquare) <$> nx
            resultString = fmap (pack . show) result
        text "from client: "
        dynText resultString
    el "p" $ do
        text "from server: "
        let xServer = decode req :: Maybe Double
        text $ case xServer of Nothing  -> "hs"
                               Just res -> "ok"

numberInput :: MonadWidget t m => m (Dynamic t (Maybe Double))
numberInput = do
    n <- textInput $ def & textInputConfig_inputType .~ "number"
                         & textInputConfig_initialValue .~ "0"
    return . fmap (readMaybe . unpack) $ _textInput_value n

-}

