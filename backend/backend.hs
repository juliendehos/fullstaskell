{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Text.Lazy (unpack)
import Network.HTTP.Types.Status (badRequest400)
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (get, json, middleware, param, scotty, status, text)
import Text.Read (readMaybe)

import Common

main :: IO ()
main = scotty 3000 $ do
    middleware logStdoutDev
    middleware simpleCors
    get "/:x" $ do
        xMaybe <- readMaybe <$> unpack <$> param "x" 
        case xMaybe of Nothing -> text "not a number" >> status badRequest400
                       Just x  -> json $ computeSquare x

