let 
  rp_ref = "6d746cc034391b64efabaff7f0c0d5a97429fc01";  # 2018-06-20
  rp = import (fetchTarball "https://github.com/reflex-frp/reflex-platform/archive/${rp_ref}.tar.gz") {};
in

rp.project ({ pkgs, ... }: {

  # useWarp = true;

  packages = {
    common = ./common;
    backend = ./backend;
    frontend_reflex = ./frontend_reflex;
    frontend_miso = ./frontend_miso;
  };

  shells = {

    ghc = [ "common" "backend" ];
    ghcjs = [ "common" "frontend_reflex" ];
    # TODO frontend_miso does not compile (
    # ghcjs = [ "common" "frontend_reflex" "frontend_miso" ];
  };

})


